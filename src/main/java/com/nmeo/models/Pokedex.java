package com.nmeo.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Pokedex {
    public static Map<String, Pokemon> pokemons = new HashMap<String, Pokemon>();

    public static void addPokemon(Pokemon pokemon) {
        pokemons.put(pokemon.pokemonName, pokemon);
    }

    public static Pokemon getPokemonById(String pName){
        return pokemons.get(pName);
    }

    public static ArrayList<Pokemon> getPokemonByName(String searchString) {
        ArrayList<Pokemon> pokemonsFound = new ArrayList<>();
        
        for (String key : pokemons.keySet()) {
            if (key.contains(searchString)) {
                pokemonsFound.add(pokemons.get(key));
            }
        }
        return pokemonsFound;
    }

    public static ArrayList<Pokemon> getPokemonByType(String searchString) {
        ArrayList<Pokemon> pokemonsFound = new ArrayList<Pokemon>();

        for (String key : pokemons.keySet()) {
            Pokemon pokemon = pokemons.get(key);
            if (pokemon.type.toString() == searchString) {
                pokemonsFound.add(pokemon);
            }
        }
        return pokemonsFound;
    }
}
