package com.nmeo.models;

import java.util.List;

public class Pokemon {
    public String pokemonName;
    public PokemonType type;
    public int lifePoints;
    public List<PokemonPowers> powers;

}
