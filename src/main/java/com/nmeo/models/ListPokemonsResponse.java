package com.nmeo.models;

import java.util.ArrayList;

public class ListPokemonsResponse {
    public ArrayList<Pokemon> result;

    public ListPokemonsResponse(ArrayList<Pokemon> pokemonsFound) {
        this.result = pokemonsFound;
    }
}