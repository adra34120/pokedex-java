package com.nmeo;

import io.javalin.Javalin;

import java.util.ArrayList;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import com.nmeo.models.ListPokemonsResponse;
import com.nmeo.models.Pokedex;
import com.nmeo.models.Pokemon;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());
    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        Javalin.create()
        .get("/api/status", ctx -> {
            logger.debug("Status handler triggered", ctx);
            ctx.status(200);
        })
        .post("/api/create", ctx -> {
            Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
            // If pokemon is not already in Pokedex
            if (Pokedex.getPokemonById(pokemon.pokemonName) == null) {
                Pokedex.addPokemon(pokemon);
                ctx.status(200);
            } else {
                ctx.status(400);
            }
        })
        .get("/api/searchByName", ctx -> {
            String nameToSearch = ctx.queryParam("name");
            ArrayList<Pokemon> pokemonsFound = Pokedex.getPokemonByName(nameToSearch);
            ctx.json(new ListPokemonsResponse(pokemonsFound));
        })
        .post("/api/modify", ctx -> {
            String typeToSearch = ctx.queryParam("type");
            ArrayList<Pokemon> pokemonsFound = Pokedex.getPokemonByType(typeToSearch);
            ctx.json(new ListPokemonsResponse(pokemonsFound));
        })
        .get("/api/searchByType", ctx -> {
            //ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);
        })
        .start(port);
    }
}